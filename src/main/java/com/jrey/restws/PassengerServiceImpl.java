package com.jrey.restws;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.springframework.stereotype.Service;

import com.jrey.restws.model.Passenger;

@Service
public class PassengerServiceImpl implements PassengerService {
	
	List<Passenger> passengers = new ArrayList<>();
	int currentId = 123;

	@Override
	public List<Passenger> getPassengers(int start, int size) {
		System.out.println(start);
		System.out.println(size);
		return passengers;
	}

	@Override
	public void addPassenger(String firstName, String lastName, String agent, HttpHeaders headers) {
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(agent);
		
		MultivaluedMap<String, String> allHeaders = headers.getRequestHeaders();
		for (String key : allHeaders.keySet()) {
			System.out.println(key + "=" + headers.getHeaderString(key));			
		}
		
		System.out.println("***COOKIES***");
		Map<String, Cookie> cookies = headers.getCookies();
		for (String key : cookies.keySet()) {
			System.out.println(key + "=" + cookies.get(key).getValue());
		}
	}

}
